<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('students');

        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sid');
            $table->string('fname');
            $table->string('lname');
            $table->date('birthdate');
            $table->string('birthplace');
            $table->integer('did');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
