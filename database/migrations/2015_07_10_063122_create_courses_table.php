<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('courses');

        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cid');
            $table->string('title');
            $table->string('description');
            $table->integer('credits');
            $table->integer('did');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
