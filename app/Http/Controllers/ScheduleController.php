<?php
/**
 * Created by PhpStorm.
 * User: Melih
 * Date: 13.7.2015
 * Time: 00:35
 */

namespace App\Http\Controllers;

use DB;
use Session;
use Redirect;

class ScheduleController extends Controller
{
    public function studentSchedule()
    {
        if (Session::get('chose_sid') == '') {

            return Redirect::to('/list');

        } else {

            $Days = array('M' => '', 'T' => '', 'W' => '', 'H' => '', 'F' => '');
            $schedule = array('08' => $Days, '09' => $Days, '10' => $Days, '11' => $Days, '12' => $Days,
                '13' => $Days, '14' => $Days, '15' => $Days, '16' => $Days);

            $scheduleSet = DB::table('schedules')
                ->join('courses', 'schedules.cid', '=', 'courses.cid')
                ->join('takes', 'takes.cid', '=', 'courses.cid')
                ->join('rooms', 'rooms.rid', '=', 'schedules.rid')
                ->join('teachs', 'takes.cid', '=', 'teachs.cid')
                ->join('teachers', 'teachers.tid', '=', 'teachs.tid')
                ->select('schedules.*', 'courses.*', 'takes.*', 'rooms.*', 'teachers.*')
                ->where('takes.sid', '=', Session::get('chose_sid'))
                ->get();


            return view('weeklySchedule')->with('scheduleSet', $scheduleSet)
                ->with('schedule', $schedule)
                ->with('whichschedule', 'Student Schedule');
        }

    }

    public function courseSchedule($cid)
    {
        if (Session::get('chose_sid') == '') {

            return Redirect::to('/list');

        } else {

            $Days = array('M' => '', 'T' => '', 'W' => '', 'H' => '', 'F' => '');
            $schedule = array('08' => $Days, '09' => $Days, '10' => $Days, '11' => $Days, '12' => $Days,
                '13' => $Days, '14' => $Days, '15' => $Days, '16' => $Days);

            $scheduleSet = DB::table('schedules')
                ->join('courses', 'schedules.cid', '=', 'courses.cid')
                ->join('takes', 'takes.cid', '=', 'courses.cid')
                ->join('rooms', 'rooms.rid', '=', 'schedules.rid')
                ->join('teachs', 'takes.cid', '=', 'teachs.cid')
                ->join('teachers', 'teachers.tid', '=', 'teachs.tid')
                ->select('schedules.*', 'courses.*', 'takes.*', 'rooms.*', 'teachers.*')
                ->where('takes.cid', '=', $cid)
                ->get();


            return view('weeklySchedule')->with('scheduleSet', $scheduleSet)
                ->with('schedule', $schedule)
                ->with('whichschedule', 'Course Schedule');
        }

    }

    public function roomSchedule($rid)
    {
        if (Session::get('chose_sid') == '') {

            return Redirect::to('/list');

        } else {

            $Days = array('M' => '', 'T' => '', 'W' => '', 'H' => '', 'F' => '');
            $schedule = array('08' => $Days, '09' => $Days, '10' => $Days, '11' => $Days, '12' => $Days,
                '13' => $Days, '14' => $Days, '15' => $Days, '16' => $Days);

            $scheduleSet = DB::table('schedules')
                ->join('courses', 'schedules.cid', '=', 'courses.cid')
                ->join('takes', 'takes.cid', '=', 'courses.cid')
                ->join('rooms', 'rooms.rid', '=', 'schedules.rid')
                ->join('teachs', 'takes.cid', '=', 'teachs.cid')
                ->join('teachers', 'teachers.tid', '=', 'teachs.tid')
                ->select('schedules.*', 'courses.*', 'takes.*', 'rooms.*', 'teachers.*')
                ->where('rooms.rid', '=', $rid)
                ->get();


            return view('weeklySchedule')->with('scheduleSet', $scheduleSet)
                ->with('schedule', $schedule)
                ->with('whichschedule', 'Room Schedule');
        }

    }

    public function teacherSchedule($tid)
    {
        if (Session::get('chose_sid') == '') {

            return Redirect::to('/list');

        } else {

            $Days = array('M' => '', 'T' => '', 'W' => '', 'H' => '', 'F' => '');
            $schedule = array('08' => $Days, '09' => $Days, '10' => $Days, '11' => $Days, '12' => $Days,
                '13' => $Days, '14' => $Days, '15' => $Days, '16' => $Days);

            $scheduleSet = DB::table('schedules')
                ->join('courses', 'schedules.cid', '=', 'courses.cid')
                ->join('takes', 'takes.cid', '=', 'courses.cid')
                ->join('rooms', 'rooms.rid', '=', 'schedules.rid')
                ->join('teachs', 'takes.cid', '=', 'teachs.cid')
                ->join('teachers', 'teachers.tid', '=', 'teachs.tid')
                ->select('schedules.*', 'courses.*', 'takes.*', 'rooms.*', 'teachers.*')
                ->where('teachers.tid', '=', $tid)
                ->get();


            return view('weeklySchedule')->with('scheduleSet', $scheduleSet)
                ->with('schedule', $schedule)
                ->with('whichschedule', 'Teacher Schedule');
        }

    }



}