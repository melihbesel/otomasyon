<?php
/**
 * Created by PhpStorm.
 * User: Melih
 * Date: 13.7.2015
 * Time: 00:35
 */

namespace App\Http\Controllers;

use App\Department;
use App\Course;
use App\Take;
use Session;
use Redirect;
use Illuminate\Http\Request;
use DB;

class CourseController extends Controller
{
    public function chooseCourse()
    {
        if (Session::get('chose_sid') == '') {

            return Redirect::to('/list');

        } else {

            $department_options = Department::lists('dname', 'id');

            return view('takeCourse')->with('department_options', $department_options);
        }

    }

    public function showCourses(Request $request)
    {
        $input = $request->all();

        $courses = Course::where('did', '=', $input["department_id"])->get();

        return view('showCourses')->with('courses', $courses);

    }

    public function chosenCourse()
    {
        if (Session::get('chose_sid') == '') {

            return Redirect::to('/list');

        } else {

            $chosencourses = DB::table('takes')
                ->join('courses', 'takes.cid', '=', 'courses.cid')
                ->select('courses.*', 'takes.*')
                ->where('takes.sid', '=', Session::get('chose_sid'))
                ->get();

            return view('chosenCourses')->with('chosencourses', $chosencourses);

        }

    }

    public function deleteCourse($sid, $cid)
    {
        Take::where('sid', '=', $sid)->where('cid', '=', $cid)->delete();

        return Redirect::to('/chosenCourse');

    }

    public function insertCourse($cid)
    {
        $take = new Take();

        $take->sid = Session::get('chose_sid');
        $take->cid = $cid;
        $take->grade = 0.00;

        $take->save();

        return Redirect::to('/chosenCourse');

    }





}