<?php
/**
 * Created by PhpStorm.
 * User: Melih
 * Date: 13.7.2015
 * Time: 00:35
 */

namespace App\Http\Controllers;

use App\Student;
use Session;
use Redirect;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function studentList()
    {

        Session::flush();
        Session::put('chose_sid', '');
        $students = Student::all();
        return view('list')->with('students', $students);

    }

    public function studentInfo()
    {

        if (Session::get('chose_sid') == '') {
            return Redirect::to('/list');
        } else {
            $students = Student::where('id', '=', Session::get('chose_id'))->get();
            return view('info')->with('students', $students);
        }

    }

    public function chooseStudent($sid)
    {
        $students = Student::where('sid', '=', $sid)->get();
        foreach($students as $s){
            Session::put('chose_sid', $s->sid);
            Session::put('chose_sname', $s->fname);
            Session::put('chose_ssurname', $s->lname);
            Session::put('chose_id', $s->id);
        }

        return view('info')->with('students', $students);

    }

    public function deleteStudent($sid)
    {
        Student::where('sid', '=', $sid)->delete();

        return Redirect::to('/list');

    }

    public function editStudent($sid)
    {
        $students = Student::where('sid', '=', $sid)->get();

        return view('update')->with('students', $students);

    }


    public function updateStudent(Request $request)
    {

        $input = $request->all();

        $student = Student::find($input["T_Sid"]);
        $student->sid = $input["T_Ssid"];
        $student->fname = $input["T_Sname"];
        $student->lname = $input["T_Ssurname"];
        $student->birthdate = $input["T_Sbirthdate"];
        $student->birthplace = $input["T_Sbirthplace"];
        $student->did = $input["T_Sdid"];

        $student->save();

        Session::put('chose_sid', $student->sid);
        Session::put('chose_sname', $student->fname);
        Session::put('chose_ssurname', $student->lname);
        Session::put('chose_id', $student->id);

        return Redirect::to('/info');

    }




}