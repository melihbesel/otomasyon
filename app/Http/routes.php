<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Student;
use App\Department;
use App\Course;
use App\Take;
use Illuminate\Http\Request;

Route::get('/', function () {
    Session::flush();
    return Redirect::to('/list');
});

Route::get('list', 'StudentController@studentList');

Route::get('info', 'StudentController@studentInfo');

Route::get('chooseCourse', 'CourseController@chooseCourse' );

Route::get('choose/{sid}', 'StudentController@chooseStudent');

Route::get('delete/{sid}', 'StudentController@deleteStudent');

Route::get('edit/{sid}', 'StudentController@editStudent');

Route::post('update/student', 'StudentController@updateStudent');

Route::post('show/courses', 'CourseController@showCourses');

Route::get('chosenCourse', 'CourseController@chosenCourse');

Route::get('delete/chosenCourse/{sid}/{cid}', 'CourseController@deleteCourse');

Route::get('insert/course/{cid}', 'CourseController@insertCourse');

Route::get('studentSchedule', 'ScheduleController@studentSchedule');

Route::get('courseSchedule/{cid}', 'ScheduleController@courseSchedule');

Route::get('roomSchedule/{rid}', 'ScheduleController@roomSchedule');

Route::get('teacherSchedule/{tid}', 'ScheduleController@teacherSchedule');


