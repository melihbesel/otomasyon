<?php
/**
 * Created by PhpStorm.
 * User: Melih
 * Date: 10.7.2015
 * Time: 09:55
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
}