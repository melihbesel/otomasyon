@extends('homepage')

@section('update')

    <div class="panel-heading">

        Student Update

    </div>

    <div class="panel-body">

        <p>

            <a id='info' href='{{URL::to("info")}}'>Back to Student Info</a>

            <!-- Table -->
        <table class="table">

            {!! Form::open(array('url' => 'update/student'))	!!}

            @foreach($students as $student)

                <tr>
                    <td>
                        {!!  Form::label('L_Sid', 'ID:') !!}
                        {!!  Form::text('T_Sid', $student->id , ['readonly']) !!}
                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>
                        {!!  Form::label('L_Ssid', 'Number:') !!}
                        {!!  Form::text('T_Ssid', $student->sid) !!}
                    </td>
                    <td>
                        {!!  Form::label('L_Sname', 'Name:') !!}
                        {!!  Form::text('T_Sname', $student->fname) !!}
                    </td>
                    <td>
                        {!!  Form::label('L_Ssurname', 'Surname:')!!}
                        {!!  Form::text('T_Ssurname', $student->lname)!!}
                    </td>
                </tr>
                <tr>
                    <td>
                        {!!  Form::label('L_Sbirthdate', 'Birthdate:')!!}
                        {!!  Form::text('T_Sbirthdate', $student->birthdate)!!}
                    </td>
                    <td>
                        {!!  Form::label('L_Sbirthplace', 'Birthplace:')!!}
                        {!!  Form::text('T_Sbirthplace', $student->birthplace)!!}
                    </td>
                    <td>
                        {!!  Form::label('L_Sdid', 'Department Id:') !!}
                        {!!  Form::text('T_Sdid', $student->did) !!}
                    </td>
                </tr>
                <tr>
                    <td id="{{URL::to("update/student")}}">
                        <input type="button" onclick="cancelUpdate()" value="Cancel">
                        <input type="submit" onclick="updateStudent(this)" value="Update">
                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>


            @endforeach

            {!! Form::close() !!}

        </table>

        <a href='{{URL::to("chooseCourse")}}'>Next to Take a Course</a>

        </p>

    </div>

@endsection

<script>
    function cancelUpdate() {
        if (confirm('Are You Sure to Cancel Update(?)') == true) {
            window.location.href = document.getElementById('info').valueOf();
        }
    }

    function updateStudent(obj){
        if(confirm('Are You Sure to Update Student(?)') == true){
            window.location.href = obj.parentNode.id;
        }
    }

</script>
