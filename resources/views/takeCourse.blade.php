@extends('homepage')

@section('takeCourse')

    <div class="panel-heading">

        Take a Course

    </div>

    <div class="panel-body">

        <p>

            <a href='{{URL::to("info")}}'>Back to Student Info</a>

        <table class="table">
            {!! Form::open(array('url' => 'show/courses'))	!!}
            <tr>
                <th>
                    Please Select a Department
                </th>
            </tr>
            <tr>
                <td>
                    {!! Form::select('department_id', $department_options) !!}
                </td>
            </tr>
            <tr>
                <td>
                    {!!  Form::submit('Show Courses') !!}
                </td>
            </tr>
            {!! Form::close() !!}
        </table>


        <a href='{{URL::to("chosenCourse")}}'>Next to Courses Taken</a>

        </p>

    </div>

@endsection