<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{!!asset('assets\bootstrap-3.3.5-dist\css\bootstrap.css')!!}">
    <title>Student Automation</title>
</head>
<body>

<div class="panel panel-default" style='width:auto;'>
    <!-- Default panel contents -->
    <div class="panel-heading">
        @if (Session::get('chose_sid') == '')
            <b>(!) Please Select a Student From Student List</b>
        @else
            <b>{{Session::get('chose_sid')}} , {{Session::get('chose_sname')}} {{Session::get('chose_ssurname')}}</b>
        @endif

    </div>
    <div class="panel-body">
        <p>

            <h3>Main Menu</h3>
            <a href='{!!URL::to("list")!!}'>Student List</a><br>
            <a href='{!!URL::to("info")!!}'>Student Info</a><br>
            <a href='{!!URL::to("chooseCourse")!!}'>Take a Course</a><br>
            <a href='{!!URL::to("chosenCourse")!!}'>Courses Taken</a><br>
            <a href='{!!URL::to("studentSchedule")!!}'>Weekly Schedule</a>


        </p>

    </div>

</div>

<div class="panel panel-default" style='width:auto;'>

    @yield('list')
    @yield('info')
    @yield('update')
    @yield('takeCourse')
    @yield('showCourses')
    @yield('chosenCourses')
    @yield('weeklySchedule')

</div>


</body>
</html>