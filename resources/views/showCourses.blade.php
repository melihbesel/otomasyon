@extends('homepage')

@section('showCourses')

    <div class="panel-heading">

        Course Insert

    </div>

    <div class="panel-body">

        <p>

            <a href='{{URL::to("chooseCourse")}}'>Back to Take a Course</a>

            <!-- Table -->
        <table class="table">

            <tr>
                <th>
                    Title
                </th>
                <th>
                    Description
                </th>
                <th>
                    Credit
                </th>
                <th>
                    *
                </th>
            </tr>

            @foreach($courses as $course)

                <tr>
                    <td>{{$course->title}}</td>
                    <td>{{$course->description}}</td>
                    <td>{{$course->credits}}</td>
                    <td id="{{URL::to("insert/course/$course->cid")}}">
                        <input type="button" onclick="insertCourse(this)" value="Insert">
                    </td>
                </tr>

            @endforeach

        </table>

            <a href='{{URL::to("chosenCourse")}}'>Next to Courses Taken</a>

        </p>

    </div>

@endsection

<script>
    function insertCourse(obj){
        if(confirm('Are You Sure to Insert Course(?)') == true){
            window.location.href = obj.parentNode.id;
        }
    }
</script>
