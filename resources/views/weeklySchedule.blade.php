@extends('homepage')

@section('weeklySchedule')

    <div class="panel-heading">

        {{$whichschedule}}

    </div>

    <div class="panel-body">

        <p>

            <a href='{{URL::to("chosenCourse")}}'>Back to Courses Taken</a>

            @foreach($scheduleSet as $row)

            <?php
            $schedule[$row->hourOfDay][$row->dayOfWeek] = "
                <a href='/local.otomasyon.com/public/courseSchedule/$row->cid'>{$row->cid} {$row->title}</a><br>
                <a href='/local.otomasyon.com/public/roomSchedule/$row->rid'>{$row->description}</a><br>
                <a href='/local.otomasyon.com/public/teacherSchedule/$row->tid'>{$row->fname} {$row->lname}</a>";
            ?>

            @endforeach


                    <!-- Table -->
        <table class="table">

            <tr>
                <th>Hour</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
            </tr>

            @foreach($schedule as $hour => $days)
                <tr>
                    <th> <?php echo $hour ?> </th>
                    <td> <?php echo $days['M'] ?></td>
                    <td> <?php echo $days['T'] ?></td>
                    <td> <?php echo $days['W'] ?></td>
                    <td> <?php echo $days['H'] ?></td>
                    <td> <?php echo $days['F'] ?></td>
                </tr>

            @endforeach

        </table>


        </p>

    </div>

@endsection