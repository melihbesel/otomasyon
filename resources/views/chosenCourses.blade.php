@extends('homepage')

@section('chosenCourses')

    <div class="panel-heading">

        Courses Taken

    </div>

    <div class="panel-body">

        <p>

            <a href='{{URL::to("chooseCourse")}}'>Back to Take a Course</a>

            <!-- Table -->
        <table class="table">

            <tr>
                <th>
                    ID
                </th>
                <th>
                    Title
                </th>
                <th>
                    Credit
                </th>
                <th>
                    *
                </th>
            </tr>

            @foreach($chosencourses as $chosencourse)

                <tr>
                    <td>{{$chosencourse->cid}}</td>
                    <td>{{$chosencourse->title}}</td>
                    <td>{{$chosencourse->credits}}</td>
                    <td id="{{URL::to("delete/chosenCourse/$chosencourse->sid/$chosencourse->cid")}}">
                        <input type="button" onclick="deleteCourse(this)" value="Delete">
                    </td>
                </tr>

            @endforeach

        </table>

            <a href='{{URL::to("studentSchedule")}}'>Next to Weekly Schedule</a>

        </p>
    </div>

@endsection

<script>
    function deleteCourse(obj){
        if(confirm('Are You Sure to Delete Course(?)') == true){
            window.location.href = obj.parentNode.id;
        }
    }
</script>

