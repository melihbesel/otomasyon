@extends('homepage')

@section('list')

    <div class="panel-heading">

        Student List

    </div>

    <div class="panel-body">

        <p>

            <!-- Table -->
        <table class="table">

            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Birthdate</th>
                <th>Birthplace</th>
                <th>Department ID</th>
                <th>*</th>
                <th>*</th>
            </tr>

            @foreach($students as $student)

                <tr>
                    <td>{{$student->sid}}</td>
                    <td>{{$student->fname}}</td>
                    <td>{{$student->lname}}</td>
                    <td>{{$student->birthdate}}</td>
                    <td>{{$student->birthplace}}</td>
                    <td>{{$student->did}}</td>
                    <td><a href="{{URL::to("choose/$student->sid")}}">Choose</a></td>
                    <td id="{{URL::to("delete/$student->sid")}}">
                        <input type="button" onclick="deleteStudent(this)" value="Delete" >
                    </td>
                </tr>


            @endforeach

        </table>

        </p>

    </div>


@endsection

<script>
    function deleteStudent(obj){
        if(confirm('Are You Sure to Delete Student(?)') == true){
            window.location.href = obj.parentNode.id;
        }
    }
</script>

