@extends('homepage')

@section('info')

    <div class="panel-heading">

        Student Info

    </div>

    <div class="panel-body">

        <p>

            <a href='{{URL::to("list")}}'>Back to Student List</a>

            <!-- Table -->
        <table class="table">

            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Birthdate</th>
                <th>Birthplace</th>
                <th>Department ID</th>
                <th>*</th>
            </tr>

            @foreach($students as $student)

                <tr>
                    <td>{{$student->sid}}</td>
                    <td>{{$student->fname}}</td>
                    <td>{{$student->lname}}</td>
                    <td>{{$student->birthdate}}</td>
                    <td>{{$student->birthplace}}</td>
                    <td>{{$student->did}}</td>
                    <td><a href='{{URL::to("edit/$student->sid")}}'>Edit</a></td>
                </tr>

            @endforeach

        </table>

            <a href='{{URL::to("chooseCourse")}}'>Next to Take a Course</a>

        </p>

    </div>

@endsection